﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "InteractComponent.h"

#include "InteractiveActor.h"
#include "Algo/Copy.h"
#include "Kismet/KismetSystemLibrary.h"


// Sets default values for this component's properties
UInteractComponent::UInteractComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;

	// ...
}

void UInteractComponent::Interact()
{
	if(!GetOwner()->HasAuthority())
	{
		// RPC server
		Server_Interact();
		return;
	}
	
	auto Interactive = Cast<IInteractive>(MostRelevantActor);
	if(!Interactive)
		return;
	if(!GetOwner())
		return;
	//TODO:Handle when cast when interaction is not possible
	if(Interactive->CanBeInteracted(this->GetOwner()))
	{
		Interactive->Interact(GetOwner());
	}
}

void UInteractComponent::AddInteractive(IInteractive* Interactive)
{
	if(!Interactive)
		return;
	if(!GetOwner())
		return;
	if(!Interactive->CanBeInteracted(GetOwner()))
		return;
	if(this->PossibleInteractives.Contains(Interactive))
		return;
	this->PossibleInteractives.Add(Interactive);
	this->RecomputeInteractiveRevelancy(1000);
}

void UInteractComponent::RemoveInteractive(IInteractive* Interactive)
{
	if(!Interactive)
		return;
	if(!this->PossibleInteractives.Contains(Interactive))
		return;;
	this->PossibleInteractives.Remove(Interactive);
	this->RecomputeInteractiveRevelancy(1000);
}


// Called when the game starts
void UInteractComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}

void UInteractComponent::Server_Interact_Implementation()
{
	Interact();
}

/**Refresh the Actor with which we interact 
 * 
 */
void UInteractComponent::RecomputeInteractiveRevelancy(float Distance)
{
	TArray<FHitResult> Results;
	 if(!ensureMsgf(this->LineSweepDelegate.IsBound(), TEXT("Delegate was not bound. Don't forget tot call SetInteractionLineSweepDelegate()")))
	 	return;
	//we choose the hit interactive actor :
	if(this->LineSweepDelegate.Execute(Distance, Results))
	{
		for (const auto& Result: Results)
		{
			//if actor null skip this one :
			if(!Result.GetActor()) continue;
			//if actor is not an interactive one :
			if(!Result.GetActor()->Implements<UInteractive>()) continue;
			//if result actor is one of the relevant interactive actor from the array possible interactives :
			for(const auto& Relevant: this->PossibleInteractives)
			{
				if(Relevant->GetInteractiveOwner() == Cast<IInteractive>(Result.GetActor())->GetInteractiveOwner())
				{
					this->MostRelevantActor = Result.GetActor();
					return;;
				}
			}
		}
		return;
	}

	//We choose the closest interactive actor : 
	this->PossibleInteractives.Sort([&](const IInteractive& A, const IInteractive& B)
	{
		return FVector::DistSquared(A.GetInteractiveLocation(), GetOwner()->GetTransform().GetLocation()) <
			FVector::DistSquared(B.GetInteractiveLocation(), GetOwner()->GetTransform().GetLocation());
	});
	if(this->PossibleInteractives.IsEmpty() || !this->PossibleInteractives[0])
	{
		return;
	}
	this->MostRelevantActor = this->PossibleInteractives[0]->GetInteractiveOwner();
}
