﻿// Fill out your copyright notice in the Description page of Project Settings.

#include"InteractiveActor.h"

#include "InteractComponent.h"
#include "Components/BoxComponent.h"
#include "Net/UnrealNetwork.h"
//TODO: add code to character to make the lineSweep (https://youtu.be/apqP8Gzd-P4?list=PLpuAT1XaeuN8rcjWB94UynsBWgNLQlKJI&t=6003)
// Sets default values
AInteractiveActor::AInteractiveActor()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	this->State = EInteractiveState::Ready;
	
	this->TriggerComp = CreateDefaultSubobject<USphereComponent>(TEXT("Trigger"));
	this->TriggerComp->SetCollisionProfileName(TEXT("Trigger"));
	this->HitBox = CreateDefaultSubobject<UBoxComponent>(TEXT("HitBox"));
	RootComponent = this->HitBox;
	this->TriggerComp->SetupAttachment(RootComponent);
}

void AInteractiveActor::Interact(AActor* InstigatorIn)
{
	if(!HasAuthority())return;
	this->State = EInteractiveState::Interacted;

	if (HasAuthority())
	{
		this->OnRep_State();
	}
	
	this->DoInteract(InstigatorIn);
	this->BP_DoInteract(InstigatorIn);
	
}

bool AInteractiveActor::CanBeInteracted(AActor* InstigatorIn)
{
	return this->State == EInteractiveState::Ready;
}

void AInteractiveActor::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(AInteractiveActor,State);
}

void AInteractiveActor::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	this->TriggerComp->OnComponentBeginOverlap.AddDynamic(this, &AInteractiveActor::HandleTriggerBeginOverlap);
	this->TriggerComp->OnComponentEndOverlap.AddDynamic(this, &AInteractiveActor::HandleTriggerEndOverlap);
}

void AInteractiveActor::DoInteract(AActor* InstigatorIn)
{
}

void AInteractiveActor::DestroyFromInteractors()
{
	for(auto& interactor: this->PossbleInteractors)
	{
		interactor->RemoveInteractive(this);
	}
}

void AInteractiveActor::OnRep_State()
{
	//Do VFX

	//disable the interactable item : 
	if(this->State == EInteractiveState::Interacted)
	{
		for(auto* Interactor: this->PossbleInteractors)
		{
			Interactor->RemoveInteractive(this);
		}
	}
}

void AInteractiveActor::HandleTriggerBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if(!OtherActor)
		return;
	auto* InteractionComp = Cast<UInteractComponent>(OtherActor->GetComponentByClass(UInteractComponent::StaticClass()));
	if(!InteractionComp)
		return;
	PossbleInteractors.Add(InteractionComp);
	InteractionComp->AddInteractive(this);
}

void AInteractiveActor::HandleTriggerEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if(!OtherActor)
		return;
	auto* InteractionComp = Cast<UInteractComponent>(OtherActor->GetComponentByClass(UInteractComponent::StaticClass()));
	if(!InteractionComp)
		return;
	PossbleInteractors.Remove(InteractionComp);
	InteractionComp->RemoveInteractive(this);
}

