﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Interactive.h"
#include "Components/ActorComponent.h"
#include "InteractComponent.generated.h"

DECLARE_DELEGATE_RetVal_TwoParams(bool, FInteractionLineSweepDelegate, float /*Interaction distance*/, TArray<FHitResult>& /*Result*/)
UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class INTERACTION_API UInteractComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UInteractComponent();

	UFUNCTION(BlueprintCallable)
	void Interact();

	void AddInteractive(IInteractive* Interactive);

	void RemoveInteractive(IInteractive* Interactive);

	void SetInteractionLineSweepDelegate(FInteractionLineSweepDelegate Delegate)
	{
		LineSweepDelegate = Delegate;
	}
protected:
	// Called when the game starts
	virtual void BeginPlay() override;
	
	//Replicated function Interact : 
	UFUNCTION(Server, Reliable)
	void Server_Interact();
	
	TArray<IInteractive*> PossibleInteractives;

	UPROPERTY(VisibleAnywhere)
	AActor* MostRelevantActor;
	
	FInteractionLineSweepDelegate LineSweepDelegate;

	UFUNCTION(BlueprintCallable)
	void RecomputeInteractiveRevelancy(float Distance);
};
