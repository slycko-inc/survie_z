﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "InteractComponent.h"
#include "Components/SphereComponent.h"
#include "GameFramework/Actor.h"
#include "InteractiveActor.generated.h"

UENUM()
enum class EInteractiveState : uint8
{
	NotReady,
	Ready,
	Interacted
};
UCLASS(Abstract)
class INTERACTION_API AInteractiveActor : public AActor, public IInteractive
{
	GENERATED_BODY()

public:
	
	// Sets default values for this actor's properties
	AInteractiveActor();

	UFUNCTION(BlueprintCallable)
	virtual void Interact(AActor* InstigatorIn) override;
	UFUNCTION(BlueprintCallable)
	virtual bool CanBeInteracted(AActor* InstigatorIn) override;
	virtual FVector GetInteractiveLocation() const override
	{
		return GetTransform().GetLocation();
	};
	virtual AActor* GetInteractiveOwner() const override
	{
		return GetParentActor();
	};

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override; 
	
	virtual void PostInitializeComponents() override;
	
protected:

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	class USphereComponent* TriggerComp;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UShapeComponent* HitBox;

	virtual void DoInteract(AActor* InstigatorIn);
	
	UFUNCTION(BlueprintCallable)
	void DestroyFromInteractors();

	UFUNCTION(BlueprintImplementableEvent)
	void BP_DoInteract(AActor* InstigatorIn);

	UPROPERTY(ReplicatedUsing=OnRep_State, BlueprintReadWrite)
	EInteractiveState State;

	UFUNCTION(BlueprintCallable)
	void OnRep_State();

	UFUNCTION()
	void HandleTriggerBeginOverlap( UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult);

	UFUNCTION()
	void HandleTriggerEndOverlap( UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	UPROPERTY()
	TArray<UInteractComponent*> PossbleInteractors;
};
