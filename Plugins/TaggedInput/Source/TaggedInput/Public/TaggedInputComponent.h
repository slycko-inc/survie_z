﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "EnhancedInputComponent.h"
#include "GameplayTagContainer.h"
#include "TaggedInputConfig.h"
#include "TaggedInputComponent.generated.h"


UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class TAGGEDINPUT_API UTaggedInputComponent : public UEnhancedInputComponent
{
	GENERATED_BODY()

public:
	template<class UserClass, typename FuncType>
	void BindActionByTag(const UTaggedInputConfig* InputConfig, const FGameplayTag InputTag, ETriggerEvent TriggerEvent,
	UserClass* Object, FuncType Func){
		check(InputConfig);
		const UInputAction* InputAction = InputConfig->FindInputActionForTag(InputTag);

		if (!InputAction) return;

		BindAction(InputAction, TriggerEvent, Object, Func);
	}
};
