﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameplayTagContainer.h"
#include "InputAction.h"
#include "UObject/Object.h"
#include "TaggedInputConfig.generated.h"

USTRUCT(BlueprintType)
struct FTaggedInputAction
{
	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly)
	const UInputAction * InputAction = nullptr;

	UPROPERTY(EditDefaultsOnly, Meta = (Categories = "InputTag"))
	FGameplayTag InputTag;
};

/**
 * 
 */
UCLASS()
class TAGGEDINPUT_API UTaggedInputConfig : public UDataAsset
{
	GENERATED_BODY()

public:
	
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Meta = (TitleProperty = "InputAction"))
	TArray<FTaggedInputAction> TaggedInputActions;
	//N.B. :Template and cons function are to be defined in .h file !!!! Otherwise => Shipping FAIL 
	const UInputAction* FindInputActionForTag(FGameplayTag Tag) const{
		for (const auto& [InputAction, InputTag] : TaggedInputActions)
		{
			if(InputAction && InputTag == Tag)
			{
				return InputAction;
			}
		}

		return nullptr;
	}
};
