﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BaseWeapon.h"
#include "Engine/DamageEvents.h"
#include "GameFramework/Actor.h"
#include "BaseGun.generated.h"



UCLASS()
class WEAPONSYSTEM_API ABaseGun : public ABaseWeapon
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ABaseGun();

	ABaseGun(int bulletAmounts);

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category="GunDetails")
	float MaxBulletDistance;
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category="GunDetails")
	float FireRate;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category="GunDetails")
	int MagazineCapacity;

	UPROPERTY(BlueprintReadOnly, Category="GunDetails")
	int CurrentBulletInMagazine;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category="GunDetails")
	int RemainingBullets;
	

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category="GunDetails")
	FPointDamageEvent PointDamageEvent;
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	virtual void GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const override;



public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
	void Reload();

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	void BP_FireFeedback(bool IsReloading, bool IsPrimaryShoot);
};
