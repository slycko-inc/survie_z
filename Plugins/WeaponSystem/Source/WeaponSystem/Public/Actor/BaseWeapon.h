﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BaseWeapon.generated.h"

UENUM()
enum class EWeaponType : uint8
{
	Primary,
	Secondary,
	Melee
};

UCLASS()
class WEAPONSYSTEM_API ABaseWeapon : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ABaseWeapon();
	
	UPROPERTY(BlueprintReadWrite,EditAnywhere, Category="Skeletal Mesh")
	USkeletalMeshComponent* MeshComponent;

	UPROPERTY(Replicated, EditAnywhere, Category="WeaponDetails")
	float Damage;
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category="GunDetails")
	FName WeaponName;
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category="GunDetails")
	EWeaponType WeaponType;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category="GunDetails")
	UAudioComponent* AudioComponent;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category="GunDetails")
	bool bIsOneHanded;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	virtual void GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;
};
