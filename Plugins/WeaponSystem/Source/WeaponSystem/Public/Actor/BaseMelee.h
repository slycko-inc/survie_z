﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BaseWeapon.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/Actor.h"
#include "BaseMelee.generated.h"


DECLARE_DELEGATE_OneParam(FOnWeaponHitDelegate, TArray<AActor*>);
UCLASS()
class WEAPONSYSTEM_API ABaseMelee : public ABaseWeapon
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ABaseMelee();

	virtual void PostInitializeComponents() override;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	FOnWeaponHitDelegate OnWeaponHitDelegate;

	void SetOnWeaponHitDelegate(FOnWeaponHitDelegate Delegate)
	{
		OnWeaponHitDelegate = Delegate;
	}

	UFUNCTION(BlueprintCallable)
	void OnActorHit(TArray<AActor*> actorsHit);
};
