﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Component/WeaponComponent.h"

#include "Net/UnrealNetwork.h"


// Sets default values for this component's properties
UWeaponComponent::UWeaponComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UWeaponComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
}

void UWeaponComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(UWeaponComponent, EquippedGun);
	DOREPLIFETIME(UWeaponComponent, EquippedMelee);
	DOREPLIFETIME(UWeaponComponent, PrimaryWeapon);
	DOREPLIFETIME(UWeaponComponent, SecondaryWeapon);
	DOREPLIFETIME(UWeaponComponent, MeleeWeapon);
	DOREPLIFETIME(UWeaponComponent, EquippedGrenade);
	DOREPLIFETIME(UWeaponComponent, MaxGrenadeAmount);
	DOREPLIFETIME(UWeaponComponent, CurrentGrenadeAmount);
	DOREPLIFETIME(UWeaponComponent, isGunEquipped);
	DOREPLIFETIME(UWeaponComponent, isGrenadeEquipped);
	
}


// Called every frame
void UWeaponComponent::TickComponent(float DeltaTime, ELevelTick TickType,
                                     FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	// ...
}

void UWeaponComponent::ToggleActor(AActor* actor, bool isEnabled)
{
	actor->SetActorHiddenInGame(!isEnabled);
	actor->SetActorEnableCollision(isEnabled);
}

void UWeaponComponent::EquipGun(ABaseGun* Weapon)
{
	//TODO: check if save the primary gun replaced by secondary gun and vic versa  
	if(EquippedGun != nullptr)
		ToggleActor(EquippedGun, false);
		// GetWorld()->DestroyActor(Cast<AActor>(EquippedGun));
	if(EquippedMelee != nullptr)
		ToggleActor(EquippedMelee, false);
		// GetWorld()->DestroyActor(Cast<AActor>(EquippedMelee));
	isGunEquipped = true;
	isMeleeEquipped= false;
	EquippedGun = Weapon;
	ToggleActor(EquippedGun , true);
	OnEquipGun.Broadcast(EquippedGun);
}

void UWeaponComponent::EquipMelee(ABaseMelee* Weapon)
{
	if(EquippedGun != nullptr)
		ToggleActor(EquippedGun, false);
	// GetWorld()->DestroyActor(Cast<AActor>(EquippedGun));
	if(EquippedMelee != nullptr)
		ToggleActor(EquippedMelee, false);
		// GetWorld()->DestroyActor(Cast<AActor>(EquippedMelee));
	isGunEquipped = false;
	isMeleeEquipped= true;
	EquippedMelee = Weapon;
	MeleeWeapon->SetOnWeaponHitDelegate(FOnWeaponHitDelegate::CreateUObject(this,&UWeaponComponent::Hit));
	ToggleActor(EquippedMelee , true);
	OnEquipMelee.Broadcast(EquippedMelee);
}

ABaseWeapon* UWeaponComponent::AddWeapon(ABaseWeapon* Weapon)
{
	ABaseWeapon* replacedWeapon = {};
	switch (Weapon->WeaponType)
	{
	case EWeaponType::Primary :
		replacedWeapon = PrimaryWeapon;
		PrimaryWeapon = Cast<ABaseGun>(Weapon);
		EquipGun(PrimaryWeapon);
		break;
	case EWeaponType::Secondary :
		replacedWeapon = SecondaryWeapon;
		SecondaryWeapon = Cast<ABaseGun>(Weapon);
		EquipGun(SecondaryWeapon);
		break;
	case EWeaponType::Melee :
		replacedWeapon = MeleeWeapon;
		MeleeWeapon = Cast<ABaseMelee>(Weapon);
		EquipMelee(MeleeWeapon);
		break;
	}

	return replacedWeapon;
}

// bool UWeaponComponent::attack()
// {
// 	if(EquippedGun != nullptr)
// 		return Shoot();
// 	else if(EquippedMelee != nullptr)
// 		return Hit();
// 	else
// 		return false;
// }

bool UWeaponComponent::Shoot()
{
	FHitResult HitResult;
	//Handle null delegate error : 
	if(!ensureMsgf(ShootLineTraceDelegate.IsBound(), TEXT("Delegate was not bound. Don't forget to call SetShootLineSweepDelegate()")))
		return false;
	
	if(EquippedGun->CurrentBulletInMagazine == 0)
	{
		OnReloadNeeded.Broadcast();
		return false;
	}
	
	EquippedGun->CurrentBulletInMagazine--;
	// EquippedGun->CurrentBullets--;
	if(ShootLineTraceDelegate.Execute(EquippedGun->MaxBulletDistance, HitResult))
	{
		return true;		
	}
	return false;
}

void UWeaponComponent::Reload()
{
	if(EquippedGun == nullptr) return;
	//magazine is already full :  
	if(EquippedGun->CurrentBulletInMagazine == EquippedGun->MagazineCapacity) return;
	if(EquippedGun->RemainingBullets <= 0) return;
	EquippedGun->Reload();
	OnReload.Broadcast(EquippedGun->bIsOneHanded);
}

void UWeaponComponent::Server_CharacterThrowGrenade_Implementation(UChildActorComponent* GrenadeRef)
{
	if(OnThrowGrenade.Execute(GrenadeRef))
	{
		CurrentGrenadeAmount--;
	}
}

bool UWeaponComponent::ThrowGrenade(UChildActorComponent* GrenadeREF)
{
	if(GetOwner() == nullptr) return false;

	if(!ensureMsgf(OnThrowGrenade.IsBound(), TEXT("Delegate was not bound. Don't forget to call SetShootLineSweepDelegate()")))
		return false;

	if(CurrentGrenadeAmount <= 0)
	{
		OnNoMoreGrenade.Broadcast();
		return false;
	}

	if(!GetOwner()->HasAuthority())
	{
		//Server rpc :
		Server_CharacterThrowGrenade(GrenadeREF);
		return true;
	}
	else
	{
		if(OnThrowGrenade.Execute(GrenadeREF))
		{
			CurrentGrenadeAmount--;
			return true;		
		}
	}
	return false;
}

void UWeaponComponent::Hit(TArray<AActor*> actorsHit)
{
	FHitResult HitResult;
	//Handle null delegate error : 
	if(!ensureMsgf(HitDelegate.IsBound(), TEXT("Delegate was not bound. Don't forget to call SetShootLineSweepDelegate()")))
		return;
	
	if(HitDelegate.Execute(actorsHit))
	{
		//Take special actions 
	}
}

