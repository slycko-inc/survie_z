﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Actor/BaseMelee.h"

#include "Components/AudioComponent.h"


// Sets default values
ABaseMelee::ABaseMelee()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

void ABaseMelee::PostInitializeComponents()
{
	Super::PostInitializeComponents();
	
}

// Called when the game starts or when spawned
void ABaseMelee::BeginPlay()
{
	Super::BeginPlay();

}


// Called every frame
void ABaseMelee::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ABaseMelee::OnActorHit(TArray<AActor*> actorsHit)
{
	if(!OnWeaponHitDelegate.IsBound())
		return;
	
	OnWeaponHitDelegate.Execute(actorsHit);
}

