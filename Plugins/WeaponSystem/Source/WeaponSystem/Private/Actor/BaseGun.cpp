﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Actor/BaseGun.h"

#include "Net/UnrealNetwork.h"



// Sets default values
ABaseGun::ABaseGun()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
}

ABaseGun::ABaseGun(int bulletAmounts)
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void ABaseGun::BeginPlay()
{
	Super::BeginPlay();
	
	PointDamageEvent.Damage = Damage;
}

void ABaseGun::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
}

// Called every frame
void ABaseGun::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ABaseGun::Reload()
{
	int AmountToAdd = MagazineCapacity - CurrentBulletInMagazine;
	if(AmountToAdd > RemainingBullets)
		AmountToAdd = RemainingBullets;
	CurrentBulletInMagazine += AmountToAdd;
	RemainingBullets -= AmountToAdd; 
}

