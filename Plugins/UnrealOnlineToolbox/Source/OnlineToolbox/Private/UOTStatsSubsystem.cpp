﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "UOTStatsSubsystem.h"
#include "Interfaces/OnlineStatsInterface.h"
#include "OnlineSubsystem.h"
#include "Interfaces/OnlineIdentityInterface.h"
#include "Interfaces/OnlineAchievementsInterface.h"

UOTStatsSubsystem::UOTStatsSubsystem()
{
}

void UOTStatsSubsystem::Initialize(FSubsystemCollectionBase& Collection)
{
	Super::Initialize(Collection);

	const auto* Subsystem = IOnlineSubsystem::Get();
	checkf(Subsystem != nullptr, TEXT("Unable to get the SubSytem"));

	StatsInterface = Subsystem->GetStatsInterface();
	checkf(StatsInterface != nullptr, TEXT("Unable to get the Stats interface"));

	IdentityInterface = Subsystem->GetIdentityInterface();
	checkf(IdentityInterface != nullptr, TEXT("Unable to get the Identity interface"));

	AchievementsInterface = Subsystem->GetAchievementsInterface();
	checkf(AchievementsInterface != nullptr, TEXT("Unable to get the Achievements interface"));
}

void UOTStatsSubsystem::Deinitialize()
{
	Super::Deinitialize();
}

void UOTStatsSubsystem::UpdateStats(FString StatName, int IngestAmount)
{
	FOnlineStatsUserUpdatedStats Stat = FOnlineStatsUserUpdatedStats(IdentityInterface->GetUniquePlayerId(0).ToSharedRef());
	
	Stat.Stats.Add(StatName, FOnlineStatUpdate(IngestAmount, FOnlineStatUpdate::EOnlineStatModificationType::Unknown));

	TArray<FOnlineStatsUserUpdatedStats> Stats;
	Stats.Add(Stat);
	StatsInterface->UpdateStats(IdentityInterface->GetUniquePlayerId(0).ToSharedRef(),Stats,FOnlineStatsUpdateStatsComplete::CreateLambda([this](
		const FOnlineError &ResultState)
		{
			if (ResultState.bSucceeded)
			{
				UE_LOG(LogTemp, Warning, TEXT("Stat update succeeded"));
				// Check `ResultState.bSucceeded`.
				OnUpdatedStatsCompleted.Broadcast(ResultState.bSucceeded);
			}
			else
			{
				OnUpdatedStatsCompleted.Broadcast(ResultState.bSucceeded);
			}
		
		}));
}