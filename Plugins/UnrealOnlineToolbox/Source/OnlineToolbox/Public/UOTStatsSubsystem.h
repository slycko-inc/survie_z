﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "OnlineSubsystem.h"
#include "UObject/Object.h"
#include "UOTStatsSubsystem.generated.h"

/**
 * 
 */
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOTOnUpdatedStatsCompletedDelegate, bool, bIsSucceeded);
UCLASS()
class ONLINETOOLBOX_API UOTStatsSubsystem : public UGameInstanceSubsystem
{
	GENERATED_BODY()

public:
	UOTStatsSubsystem();

	virtual void Initialize(FSubsystemCollectionBase& Collection) override;
	
	virtual void Deinitialize() override;

	UFUNCTION(BlueprintCallable, Category="Stats")
	void UpdateStats(FString StatName, int IngestAmount);

	UPROPERTY(BlueprintAssignable)
	FOTOnUpdatedStatsCompletedDelegate OnUpdatedStatsCompleted;

private:
	
	IOnlineStatsPtr StatsInterface;
	IOnlineIdentityPtr IdentityInterface;
	IOnlineAchievementsPtr AchievementsInterface;
};
