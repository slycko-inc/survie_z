﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "SZ_LobbyGameMode.h"

#include "GameFramework/GameStateBase.h"


void ASZ_LobbyGameMode::OnPostLogin(AController* NewPlayer)
{
	
	OnPostLoginDelegate.Broadcast();
	Super::OnPostLogin(NewPlayer);

	if(GameState->PlayerArray.Num() >= this->MinimumNumberOfPlayers && bAutoLoadGameWorld)
	{
		TravelToGame();
		
		//Client travel : 
		//Cast<APlayerController>(NewPlayer)->ClientTravel(TEXT("127.0.0.1"), TRAVEL_Absolute);
	}

}

void ASZ_LobbyGameMode::TravelToGame()
{
	GetWorld()->ServerTravel(this->GameWorldName);
}
