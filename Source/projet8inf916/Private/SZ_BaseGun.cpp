﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "SZ_BaseGun.h"

#include "Net/UnrealNetwork.h"
#include "Particles/ParticleSystemComponent.h"


// Sets default values
ASZ_BaseGun::ASZ_BaseGun()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MeshComponent = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("SkeletalMeshComponent"));

	MeshComponent->SetupAttachment(RootComponent);

	AudioComponent = CreateDefaultSubobject<UAudioComponent>(TEXT("AudioComponent"));
	
	AudioComponent->SetupAttachment(RootComponent);

}

// Called when the game starts or when spawned
void ASZ_BaseGun::BeginPlay()
{
	Super::BeginPlay();

	PointDamageEvent.Damage = Damage;
	CurrentBulletInMagazine = MagazineCapacity;
}

void ASZ_BaseGun::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ASZ_BaseGun,Damage);
}

// Called every frame
void ASZ_BaseGun::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ASZ_BaseGun::Reload()
{
	CurrentBulletInMagazine = MagazineCapacity;
}

