﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Controllers/SZ_PlayerController.h"

#include "GameFramework/GameStateBase.h"

void ASZ_PlayerController::ServerRequestServerTime_Implementation(float RequestWorldTime)
{
	const float CurrentServerTime = GetWorld()->GetGameState()->GetServerWorldTimeSeconds();
	ClientResponseServerTime(RequestWorldTime, CurrentServerTime);
}

void ASZ_PlayerController::ClientResponseServerTime_Implementation(float RequestWorldTime, float CurrentServerTime)
{
	const double Rtt = GetWorld()->GetTimeSeconds() - RequestWorldTime;
	ServerTime = CurrentServerTime + (Rtt / 2.0f);
}

void ASZ_PlayerController::ReceivedPlayer()
{
	Super::ReceivedPlayer();

	if(!IsLocalController()) return;
	ServerRequestServerTime(GetWorld()->GetTimeSeconds());
}

void ASZ_PlayerController::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	//if(ServerTime == 0) return;
	
	ServerTime += DeltaSeconds;
}
