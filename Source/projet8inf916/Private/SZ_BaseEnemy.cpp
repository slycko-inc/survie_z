﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "SZ_BaseEnemy.h"

#include "SZ_TPSCharacter.h"
#include "Controllers/SZ_PlayerController.h"
#include "Net/UnrealNetwork.h"


// Sets default values
ASZ_BaseEnemy::ASZ_BaseEnemy()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
	State = EEnemyState::Alive;
}

// Called when the game starts or when spawned
void ASZ_BaseEnemy::BeginPlay()
{
	Super::BeginPlay();
	OnTakePointDamage.AddDynamic(this,&ASZ_BaseEnemy::TakePointDamage);
	OnTakeRadialDamage.AddDynamic(this, &ASZ_BaseEnemy::TakeRadialDamage);
}

void ASZ_BaseEnemy::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ASZ_BaseEnemy, State);
	DOREPLIFETIME(ASZ_BaseEnemy, Life);
}

void ASZ_BaseEnemy::OnRep_State() const
{
	if (State == EEnemyState::Dead)
	{
		OnDeathDelegate.Broadcast();
	}
}

void ASZ_BaseEnemy::OnRep_Life(float OldLife)
{
	BP_LifeChange(OldLife, Life);
}

void ASZ_BaseEnemy::DecreaseLife(float Delta)
{
	float oldLife = Life;
	Life -= Delta;

	if(HasAuthority()) {OnRep_Life(oldLife);}
}

void ASZ_BaseEnemy::TakePointDamage(AActor* DamagedActor, float Damage, AController* InstigatedBy, FVector HitLocation, UPrimitiveComponent* FHitComponent, FName BoneName, FVector ShotFromDirection, const UDamageType* DamageType, AActor* DamageCauser)
{
	ASZ_BaseEnemy* Enemy = Cast<ASZ_BaseEnemy>(DamagedActor);
	
	if(Enemy == nullptr) return;
	
	if (BoneName == "Head")
	{
		// Make sure that damage is from a player
		ASZ_PlayerController* PlayerController = Cast<ASZ_PlayerController>(DamageCauser);

		if(PlayerController == nullptr) return;
		
		ASZ_TPSCharacter* Player = Cast<ASZ_TPSCharacter>(PlayerController->GetPawn());

		if(Player == nullptr) return;

		float _damage = Damage * Player->HeadShotModifier;

		DecreaseLife(_damage);

		OnTakeDamage.Broadcast(Damage);
		BP_TakeDamage(DamageCauser);
		
		if (Enemy->Life <= 0)
		{
			Enemy->State = EEnemyState::Dead;
			BP_DeathCauser(Player);
			OnRep_State();

			// Delegate for update stat in blueprint
			Player->OnHeadShot.Broadcast();
		}
	}
	else
	{
		DecreaseLife(Damage);
		BP_TakeDamage(DamageCauser);
		OnTakeDamage.Broadcast(Damage);
		if (Enemy->Life <= 0)
		{
			Enemy->State = EEnemyState::Dead;
			ASZ_PlayerController* PlayerController = Cast<ASZ_PlayerController>(DamageCauser);

			if(PlayerController == nullptr) return;
		
			ASZ_TPSCharacter* Player = Cast<ASZ_TPSCharacter>(PlayerController->GetPawn());

			if(Player == nullptr) return;

			BP_DeathCauser(Player);
			OnRep_State();
		}
	}
}

void ASZ_BaseEnemy::TakeRadialDamage(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, FVector Origin, FHitResult HitInfo, class AController* InstigatedBy, AActor* DamageCauser )
{
	UE_LOG(LogTemp, Warning, TEXT("Take radial damage"));
	
	ASZ_BaseEnemy* Enemy = Cast<ASZ_BaseEnemy>(DamagedActor);
	
	if(Enemy == nullptr) return;
	Damage;
	//TODO: A enlever quand les radial damage vont fonctionner. Demander a valere
	//Enemy->Life -= 150;
	// ActualDamage = FMath::Lerp(RadialDamageEvent.Params.MinimumDamage, ActualDamage, FMath::Max(0.f, RadialDamageScale));
	// CAlculer le fallof a la main pour avoir le bon damage. Utilsier l'origin
	DecreaseLife(Damage);
	OnTakeDamage.Broadcast(Damage);
	

	UE_LOG(LogTemp, Error, TEXT("Enemy life : %f"), Enemy->Life);
	if (Enemy->Life <= 0)
	{
		Enemy->State = EEnemyState::Dead;
		
		ASZ_PlayerController* PlayerController = Cast<ASZ_PlayerController>(DamageCauser);

		if(PlayerController == nullptr) return;
		
		ASZ_TPSCharacter* Player = Cast<ASZ_TPSCharacter>(PlayerController->GetPawn());
		
		BP_DeathCauser(Player);
		
		// Delegate for update stat in blueprint
		Player->OnEnemyDieByGrenade.Broadcast();
		
		OnRep_State();
	}
	
}

// Called every frame
void ASZ_BaseEnemy::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

float ASZ_BaseEnemy::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator,
	AActor* DamageCauser)
{
	return Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
}

void ASZ_BaseEnemy::TakeExplosionDamage(float Damage, FVector Vector,AActor* DamageCauser)
{
	DecreaseLife(Damage);
	OnTakeDamage.Broadcast(Damage);
	if (Life <= 0)
	{
		State = EEnemyState::Dead;
		//Cast always true cause call from player
		BP_DeathCauser(Cast<ASZ_TPSCharacter>(DamageCauser));
		OnRep_State();
	}
}

void ASZ_BaseEnemy::TakeGenericDamage(float Damage,AActor* DamageCauser)
{
	DecreaseLife(Damage);
	OnTakeDamage.Broadcast(Damage);
	if(Life <= 0)
	{
		State = EEnemyState::Dead;
		//Cast always true cause call from player
		BP_DeathCauser(Cast<ASZ_TPSCharacter>(DamageCauser));
		OnRep_State();
	}
}
