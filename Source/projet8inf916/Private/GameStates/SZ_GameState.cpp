﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "GameStates/SZ_GameState.h"

#include "Controllers/SZ_PlayerController.h"

float ASZ_GameState::GetServerWorldTimeSeconds() const
{
	auto* PlayerController = GetGameInstance()->GetFirstLocalPlayerController(GetWorld());
	const auto* SzPlayerController = Cast<ASZ_PlayerController>(PlayerController);

	if(SzPlayerController == nullptr)
	{
		return GetWorld()->GetTimeSeconds();
	}

	return SzPlayerController->GetServerTime();
}
