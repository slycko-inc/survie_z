﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "GameStates/SZ_WaveTimerGameState.h"

#include "Net/UnrealNetwork.h"

void ASZ_WaveTimerGameState::SetWaveTime(float Time)
{
	WaveTime = Time;

	if(HasAuthority())
	{
		OnRep_WaveTime();
	}
}

void ASZ_WaveTimerGameState::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ASZ_WaveTimerGameState, WaveTime);
}

void ASZ_WaveTimerGameState::OnRep_WaveTime()
{
	if(WaveTime - GetServerWorldTimeSeconds() <= 0)
	{
		// Broadcast for server travel		
	}
	OnWaveTimeReplicated.Broadcast(WaveTime, WaveTime - GetServerWorldTimeSeconds()); 
}
