﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "SZ_ZombieWavesGameMode.h"

#include "Controllers/SZ_PlayerController.h"
#include "GameStates/SZ_GameState.h"
#include "GameStates/SZ_WaveTimerGameState.h"

ASZ_ZombieWavesGameMode::ASZ_ZombieWavesGameMode()
{
	GameStateClass = ASZ_WaveTimerGameState::StaticClass();
	PlayerControllerClass = ASZ_PlayerController::StaticClass();
}

void ASZ_ZombieWavesGameMode::TravelToLobby()
{
	GetWorld()->ServerTravel(LobbyWorldName);
}

void ASZ_ZombieWavesGameMode::BeginPlay()
{
	Super::BeginPlay();

	auto* MyGameState = Cast<ASZ_WaveTimerGameState>(GameState);
	if(!ensureMsgf(MyGameState, TEXT("Invalid GameState"))) return;

	MyGameState->SetWaveTime(GetWorld()->GetTimeSeconds() + WavePeriodTime);
}
