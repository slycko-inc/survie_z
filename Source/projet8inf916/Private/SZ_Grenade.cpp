﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "SZ_Grenade.h"

#include "SZ_BaseEnemy.h"
#include "SZ_TPSCharacter.h"
#include "Net/UnrealNetwork.h"

bool ASZ_Grenade::ExecuteSweep(TArray<FHitResult>& OutResults, FRadialDamageEvent DamageEvent)
{
	FCollisionShape Shape = FCollisionShape::MakeSphere(DamageEvent.Params.OuterRadius);
	const FCollisionQueryParams TraceParam(FName(TEXT("TraceParam")),false,this);
	
	return GetWorld()->SweepMultiByChannel(OutResults,
		GetActorLocation(),
		GetActorLocation(),
		FQuat::Identity,
		ECC_WorldDynamic,
		Shape, TraceParam);
}

void ASZ_Grenade::BeginPlay()
{
	Super::BeginPlay();

	RadialDamageEvent.Params = RadialDamageParams;
	State = EGrenadeState::Ready;
	SphereComponentGrenadeRange->SetCollisionProfileName(TEXT("Trigger"));
	SphereComponentGrenadeRange->SetGenerateOverlapEvents(true);
	SphereComponentGrenadeRange->SetSphereRadius(RadialDamageEvent.Params.OuterRadius);
}

void ASZ_Grenade::OnExplode()
{
	if(GetOwner() == nullptr) return;

	UWorld* World = GetWorld();
	if(!World)
		return;

	//TArray<FHitResult> OutResults;
	//DrawDebugSphere(World,GetActorLocation(),RadialDamageEvent.Params.InnerRadius,32,FColor::Red,false,10);
	//DrawDebugSphere(World,GetActorLocation(),RadialDamageEvent.Params.OuterRadius,32,FColor::Blue,false,10);

	TArray<AActor*> EnemyHitsByGrenade;
	SphereComponentGrenadeRange->GetOverlappingActors(EnemyHitsByGrenade,ASZ_BaseEnemy::StaticClass());

	FHitResult HitResult;
	TArray<FHitResult>HitResults;
	for (auto HitsByGrenade : EnemyHitsByGrenade)
	{
		FHitResult temp;
		if(HitsByGrenade->GetComponentByClass(UPrimitiveComponent::StaticClass()) == nullptr) continue;
		temp.Component = MakeWeakObjectPtr(Cast<UPrimitiveComponent>(HitsByGrenade->GetComponentByClass(UPrimitiveComponent::StaticClass())));
		HitResults.Add(temp);
	}
	
	if (!EnemyHitsByGrenade.IsEmpty())
	{
		RadialDamageEvent.Origin = GetActorLocation();
		RadialDamageEvent.ComponentHits = HitResults;
		
		for (const auto& Actor: EnemyHitsByGrenade)
		{

			ASZ_BaseEnemy* Enemy = Cast<ASZ_BaseEnemy>(Actor);

			if(Enemy == nullptr) continue;

			UE_LOG(LogTemp, Warning, TEXT("enemy toucher"));
			if(!GetOwner()->HasAuthority())
			{
				ASZ_TPSCharacter* Causer = Cast<ASZ_TPSCharacter>(GetOwner());

				if(Causer == nullptr) return;

				// Causer->WeaponComponent->Server_EnemyTakeRadialDamage_Implementation(Enemy, this);
				State = EGrenadeState::Exploded;
			}
			else
			{
				Enemy->TakeDamage(RadialDamageEvent.Params.BaseDamage,RadialDamageEvent,GetOwner()->GetInstigatorController(),GetOwner());
				State = EGrenadeState::Exploded;
			}
		}

		if (State==EGrenadeState::Ready)
		{
			State = EGrenadeState::Exploded;
		}
	}
	
	if (State==EGrenadeState::Ready)
	{
		State = EGrenadeState::Exploded;
	}
	OnRep_State();
}

void ASZ_Grenade::OnRep_State() const
{
	bisOnExploded.Broadcast();
}

ASZ_Grenade::ASZ_Grenade()
{
	SphereComponent = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComponent"));
	StaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMeshComponent"));

	SphereComponent->SetupAttachment(RootComponent);
	StaticMeshComponent->SetupAttachment(SphereComponent);

	SphereComponentGrenadeRange = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComponentGrenadeRange"));
	SphereComponentGrenadeRange->SetupAttachment(SphereComponent);
}

void ASZ_Grenade::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(ASZ_Grenade, State);
}
