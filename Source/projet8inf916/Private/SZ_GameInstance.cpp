﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "SZ_GameInstance.h"

#if PLATFORM_WINDOWS
#include "Windows/WindowsHWrapper.h"
#endif

void USZ_GameInstance::Init()
{
	Super::Init();

	FCoreUObjectDelegates::PreLoadMap.AddUObject(this, &USZ_GameInstance::BeginLoadingScreen);
	FCoreUObjectDelegates::PostLoadMapWithWorld.AddUObject(this, &USZ_GameInstance::EndLoadingScreen);
}

bool USZ_GameInstance::IsAzertyKeyboard()
{
	#if PLATFORM_WINDOWS
	switch (PRIMARYLANGID(LOWORD( GetKeyboardLayout( 0 ) )))
	{
	case LANG_FRENCH:
		return true;
		break;
	default:
		return false;
		break;
	}
#else
	return false;
#endif
}
