﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "SZ_TPSCharacter.h"
#include "CollisionDebugDrawingPublic.h"
#include "SZ_BaseEnemy.h"
#include "Camera/CameraComponent.h"
#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"
// #include "TaggedInputComponent.h"
#include "Kismet/KismetMathLibrary.h"

UE_DEFINE_GAMEPLAY_TAG(TAG_Input_Move, "Input.Move")
UE_DEFINE_GAMEPLAY_TAG(TAG_Input_Look, "Input.Look")

// Sets default values
ASZ_TPSCharacter::ASZ_TPSCharacter()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	this->InteractComponent = CreateDefaultSubobject<UInteractComponent>(TEXT("Interact"));

	this->EquipmentComponent = CreateDefaultSubobject<UWeaponComponent>(TEXT("WeaponComponent"));

}

void ASZ_TPSCharacter::PostInitializeComponents()
{
	Super::PostInitializeComponents();
	

	InteractComponent->SetInteractionLineSweepDelegate(FInteractionLineSweepDelegate::CreateUObject(this, &ASZ_TPSCharacter::HandleInteractionLineSweep));
	EquipmentComponent->SetShootLineSweepDelegate(FShootLineTraceDelegate::CreateUObject(this,&ASZ_TPSCharacter::HandleShootLineTrace));
	EquipmentComponent->SetHitDelegate(FHitDelegate::CreateUObject(this,&ASZ_TPSCharacter::HandleHit));
	EquipmentComponent->SetOnThrowGrenadeDelegate(FOnThrowGrenadeDelegate::CreateUObject(this, &ASZ_TPSCharacter::HandleThrowGrenade));
}

bool ASZ_TPSCharacter::HandleInteractionLineSweep(float Distance, TArray<FHitResult>& OutResults)
{
	UWorld* World = GetWorld();
	if(!World)
		return false;

	FVector EyeLocation;
	FRotator EyeRotation;
	GetActorEyesViewPoint(EyeLocation, EyeRotation);
	//EyeLocation = EyeLocation + (EyeRotation.Vector()*200);
	FVector End = EyeLocation + (EyeRotation.Vector() * Distance);
	FCollisionShape Shape = FCollisionShape::MakeSphere(25);
	TArray<FHitResult> HitResults;
	//DrawLineTraces(World, EyeLocation, End, HitResults, 10);
	const FCollisionQueryParams TraceParam(FName(TEXT("TraceParam")),false,this);
	return World->SweepMultiByChannel(OutResults,
		EyeLocation,
		End,
		FQuat::Identity,
		ECC_WorldDynamic,
		Shape, TraceParam);
}

void ASZ_TPSCharacter::Server_InflictShootDamage_Implementation(ASZ_BaseEnemy* Enemy,
	ASZ_TPSCharacter* Asz_TPSCharacter)
{
	if(Enemy == nullptr || Asz_TPSCharacter == nullptr)
		return;
	Enemy->TakeDamage(Asz_TPSCharacter->EquipmentComponent->EquippedGun->PointDamageEvent.Damage,EquipmentComponent->EquippedGun->PointDamageEvent,GetOwner()->GetInstigatorController(),GetOwner());
}

void ASZ_TPSCharacter::Server_InflictGenericDamage_Implementation(ASZ_BaseEnemy* Enemy,
	ASZ_TPSCharacter* Asz_TPSCharacter)
{
	if(Enemy == nullptr || Asz_TPSCharacter == nullptr)
		return;
	Enemy->TakeGenericDamage(EquipmentComponent->MeleeWeapon->Damage, this);
}

bool ASZ_TPSCharacter::HandleShootLineTrace(float Distance, FHitResult& OutResults)
{
	UWorld* World = GetWorld();
	if(!World)
		return false;

	FVector EyeLocation;
	FRotator EyeRotation;
	GetActorEyesViewPoint(EyeLocation, EyeRotation);
	
	//FVector End = EyeLocation + (EyeRotation.Vector() * Distance);

	FVector Start = GetFollowCamera()->GetComponentLocation();
	
	//FVector End = EyeLocation + (GetFollowCamera()->GetForwardVector() * Distance);
	//TODO add shoot error on the linetrace  
	FVector End = Start + (UKismetMathLibrary::GetForwardVector(GetFollowCamera()->GetComponentRotation()) * Distance);
	
	
	TArray<FHitResult> HitResults;
	// DrawLineTraces(World, Start, End, HitResults, 10);
	const FCollisionQueryParams TraceParam(FName(TEXT("TraceParam")),false,this);
	bool isHit = World->LineTraceSingleByChannel(OutResults,Start,End,ECC_WorldDynamic,TraceParam);
	
	if(isHit)
	{
		if(!OutResults.GetActor()) return true;

		EquipmentComponent->EquippedGun->PointDamageEvent.HitInfo = OutResults;
		
		ASZ_BaseEnemy* Enemy = Cast<ASZ_BaseEnemy>(OutResults.GetActor());

		if(Enemy == nullptr) return true;

		// Call TakeDamage on enemy

		if(!GetOwner()->HasAuthority())
		{
			Server_InflictShootDamage(Enemy, this);
		}
		else
		{
			Enemy->TakeDamage(EquipmentComponent->EquippedGun->PointDamageEvent.Damage,EquipmentComponent->EquippedGun->PointDamageEvent,GetOwner()->GetInstigatorController(),GetOwner());
		}
	}
	
	return isHit;
}

bool ASZ_TPSCharacter::HandleHit(TArray<AActor*> actorsHit)
{

	if(GetOwner() == nullptr) return false;

	UWorld* World = GetWorld();
	if(!World)
		return false;

	
	for(AActor* ennemyHit : actorsHit)
	{
		ASZ_BaseEnemy* Enemy = Cast<ASZ_BaseEnemy>(ennemyHit);
		if(Enemy == nullptr) continue;;

		// Call TakeDamage on enemy

		if(!GetOwner()->HasAuthority())
		{
			Server_InflictGenericDamage(Enemy, this);
		}
		else
		{
			Enemy->TakeGenericDamage(EquipmentComponent->MeleeWeapon->Damage,this);
		}
	}
	return true;
	
}

void ASZ_TPSCharacter::Server_InflictExplosionDamage_Implementation(ASZ_BaseEnemy* targetEnemy, float Damage, FVector Origin)
{
	if(targetEnemy == nullptr)
		return;
	targetEnemy->TakeExplosionDamage(Damage, Origin, this);
}

bool ASZ_TPSCharacter::HandleThrowGrenade(UChildActorComponent* GrenadeREF)
{
	FActorSpawnParameters SpawnParameters;
	
	SpawnParameters.Instigator = this;
	SpawnParameters.Owner = this;
	
	ABaseGrenade* NewGrenade = GetWorld()->SpawnActor<ABaseGrenade>(GrenadeToSpawn,GrenadeREF->GetComponentLocation(),GetActorRotation(), SpawnParameters);

	FVector DirectionUnitVector = UKismetMathLibrary::GetDirectionUnitVector(GetActorLocation(),NewGrenade->GetActorLocation());

	FVector MultiplyResult = UKismetMathLibrary::Multiply_VectorInt(DirectionUnitVector, GrenadeThrowSpeed);
	
	NewGrenade->SphereComponent->SetPhysicsLinearVelocity(MultiplyResult);
	NewGrenade->SetGrenadeExplosionDelegate(FGrenadeExplosionDelegate::CreateUObject(this, &ASZ_TPSCharacter::HandleGrenadeExplosion));
	GetOwner()->GetWorldTimerManager().SetTimer(ExplodeTimerHandle,NewGrenade,&ABaseGrenade::OnExplode,1,false,2);

	return true;
}

bool ASZ_TPSCharacter::HandleGrenadeExplosion(USphereComponent* SphereComponentGrenadeRange, FRadialDamageEvent& RadialDamageEvent, EBaseGrenadeState State,AActor* grenadeActor)
{
	if(GetOwner() == nullptr) return false;

	UWorld* World = GetWorld();
	if(!World)
		return false;

	
	TArray<AActor*> EnemyHitsByGrenade;
	SphereComponentGrenadeRange->GetOverlappingActors(EnemyHitsByGrenade,ASZ_BaseEnemy::StaticClass());

	FHitResult HitResult;
	TArray<FHitResult>HitResults;
	for (auto HitsByGrenade : EnemyHitsByGrenade)
	{
		FHitResult temp;
		if(HitsByGrenade->GetComponentByClass(UPrimitiveComponent::StaticClass()) == nullptr) continue;
		temp.Component = MakeWeakObjectPtr(Cast<UPrimitiveComponent>(HitsByGrenade->GetComponentByClass(UPrimitiveComponent::StaticClass())));
		HitResults.Add(temp);
	}
	
	if (!EnemyHitsByGrenade.IsEmpty())
	{
		RadialDamageEvent.Origin = GetActorLocation();
		RadialDamageEvent.ComponentHits = HitResults;
		
		for (const auto& Actor: EnemyHitsByGrenade)
		{

			ASZ_BaseEnemy* Enemy = Cast<ASZ_BaseEnemy>(Actor);

			if(Enemy == nullptr) continue;

			UE_LOG(LogTemp, Warning, TEXT("enemy toucher"));
			if(!HasAuthority())
			{
				Server_InflictExplosionDamage(Enemy, RadialDamageEvent.Params.BaseDamage, grenadeActor->GetActorLocation());
				State = EBaseGrenadeState::Exploded;
			}
			else
			{
				Enemy->TakeExplosionDamage( RadialDamageEvent.Params.BaseDamage, grenadeActor->GetActorLocation(), this);

				State = EBaseGrenadeState::Exploded;
			}
		}

		if (State==EBaseGrenadeState::Ready)
		{
			State = EBaseGrenadeState::Exploded;
		}
	}
	
	if (State==EBaseGrenadeState::Ready)
	{
		State = EBaseGrenadeState::Exploded;
	}
	return true;
}

// Called when the game starts or when spawned
void ASZ_TPSCharacter::BeginPlay()
{
	Super::BeginPlay();

	const APlayerController* PlayerController = Cast<APlayerController>(Controller);
	if (!PlayerController) return;
	
	const auto* LocalPlayer = PlayerController->GetLocalPlayer();
	if (!LocalPlayer) return;
	
	auto* EnhancedInputSubsystem = LocalPlayer->GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(LocalPlayer);
	if (!ensureMsgf(EnhancedInputSubsystem,
					TEXT("A Local player was detected but it is not configured to use Enhanced Input"))) return;

	const auto Layout = GetGameInstance()->GetSubsystem<UTaggedInputSubsystem>()->CheckKeyboardLayout();
	
	for (const auto& [InputLayout, MappingContext] : InputContexts)
	{
		if (MappingContext && InputLayout == Layout)
		{
			EnhancedInputSubsystem->AddMappingContext(MappingContext, 0);
		}
	}
	
}

// Called every frame
void ASZ_TPSCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void ASZ_TPSCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	// Set up action bindings
	auto* TaggedInputComponent = CastChecked<UTaggedInputComponent>(PlayerInputComponent);
	if(TaggedInputComponent == nullptr)
		return;
	
	//Moving
	TaggedInputComponent->BindActionByTag(InputConfig, TAG_Input_Move, ETriggerEvent::Triggered, this,
										  &ASZ_TPSCharacter::Move);
}

