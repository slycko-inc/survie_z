// Fill out your copyright notice in the Description page of Project Settings.

using UnrealBuildTool;

public class projet8inf916 : ModuleRules
{
	public projet8inf916(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;
	
		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "Interaction",  "EnhancedInput", "OnlineSubsystemEOS", "WeaponSystem","TaggedInput", "GameplayTags"});


		// Uncomment if you are using Slate UI
		// PrivateDependencyModuleNames.AddRange(new string[] { "Slate", "SlateCore" });
		
		// Uncomment if you are using online features
		PrivateDependencyModuleNames.Add("OnlineSubsystem");

		// To include OnlineSubsystemSteam, add it to the plugins section in your uproject file with the Enabled attribute set to true
	}
}
