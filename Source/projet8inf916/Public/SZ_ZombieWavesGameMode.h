﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SZ_GameMode.h"
#include "UObject/Object.h"
#include "SZ_ZombieWavesGameMode.generated.h"

/**
 * 
 */
UCLASS()
class PROJET8INF916_API ASZ_ZombieWavesGameMode : public ASZ_GameMode
{
	GENERATED_BODY()

public:
	ASZ_ZombieWavesGameMode();

	UFUNCTION(BlueprintCallable)
	void TravelToLobby();

protected:
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Wave)
	float WavePeriodTime;
};
