// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameMode.h"
#include "SZ_GameMode.generated.h"

/**
 * 
 */
UCLASS()
class PROJET8INF916_API ASZ_GameMode : public AGameMode
{
	GENERATED_BODY()
public:

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FString GameWorldName;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FString LobbyWorldName;
	
	UFUNCTION(BlueprintCallable)
	virtual void DoServerTravel();
};
