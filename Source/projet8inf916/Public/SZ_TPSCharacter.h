﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "InteractComponent.h"
#include "NativeGameplayTags.h"
#include "TaggedInputConfig.h"
 #include "TaggedKeyboardContext.h"
#include "GameFramework/Character.h"
#include "projet8inf916/TP_ThirdPerson/TP_ThirdPersonCharacter.h"
#include "Component/WeaponComponent.h"
#include "SZ_TPSCharacter.generated.h"


UE_DECLARE_GAMEPLAY_TAG_EXTERN(TAG_Input_Move);


DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnEnemyDieByGrenadeDelegate);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FSZOnHeadShotDelegate);

UCLASS()
class PROJET8INF916_API ASZ_TPSCharacter : public ATP_ThirdPersonCharacter
{
	GENERATED_BODY()

	/** Input Config **/
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Input, meta=(AllowPrivateAccess = "true"))
	UTaggedInputConfig* InputConfig;
	
	FTimerHandle ExplodeTimerHandle;
public:
	// Sets default values for this character's properties
	ASZ_TPSCharacter();

	UPROPERTY(EditAnywhere)
	TSubclassOf<class ABaseGrenade> GrenadeToSpawn;

	virtual void PostInitializeComponents() override;
	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	//Interaction component :
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	UInteractComponent* InteractComponent;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	UWeaponComponent* EquipmentComponent;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category="Grenade")
	int GrenadeThrowSpeed;
	
	//To handle interaction line Sweep :
	UFUNCTION(BlueprintCallable)
	bool HandleInteractionLineSweep(float Distance, TArray<FHitResult>& OutResults);

	UFUNCTION(Server, Reliable)
	void Server_InflictShootDamage(ASZ_BaseEnemy* Enemy, ASZ_TPSCharacter* Asz_TPSCharacter);
	
	UFUNCTION(Server, Reliable)
	void Server_InflictGenericDamage(ASZ_BaseEnemy* Enemy, ASZ_TPSCharacter* Asz_TPSCharacter);
	
	UFUNCTION(BlueprintCallable)
	bool HandleShootLineTrace(float Distance, FHitResult& OutResults);
	
	UFUNCTION(BlueprintCallable)
	bool HandleHit(TArray<AActor*> actorsHit);

	UFUNCTION(Server, Reliable)
	void Server_InflictExplosionDamage(ASZ_BaseEnemy* targetEnemy, float Damage, FVector Origin);

	//To handle grenade throwing
	UFUNCTION(BlueprintCallable)
	bool HandleThrowGrenade(UChildActorComponent* GrenadeREF);

	//to handle grenade explosion damage
	UFUNCTION(BlueprintCallable)
	bool HandleGrenadeExplosion(USphereComponent* SphereComponentGrenadeRange, FRadialDamageEvent& RadialDamageEvent, EBaseGrenadeState State, AActor* grenadeActor);


	UPROPERTY(BlueprintAssignable)
	FOnEnemyDieByGrenadeDelegate OnEnemyDieByGrenade;

	UPROPERTY(BlueprintAssignable)
	FSZOnHeadShotDelegate OnHeadShot;

	UPROPERTY(BlueprintReadWrite,EditAnywhere, Category="HeadShotModifier")
	float HeadShotModifier;
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	/** Input Contexts */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category=Input)
	TArray<FTaggedKeyboardContext> InputContexts;
	
};
