﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/AudioComponent.h"
#include "Engine/DamageEvents.h"
#include "GameFramework/Actor.h"
#include "SZ_BaseGun.generated.h"

UCLASS()
class PROJET8INF916_API ASZ_BaseGun : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ASZ_BaseGun();

	UPROPERTY(BlueprintReadWrite,EditAnywhere, Category="Skeletal Mesh")
	USkeletalMeshComponent* MeshComponent;

	UPROPERTY(Replicated, EditAnywhere, Category="GunDetails")
	float Damage;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category="GunDetails")
	float MaxBulletDistance;
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category="GunDetails")
	float FireRate;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category="GunDetails")
	int MagazineCapacity;

	UPROPERTY(BlueprintReadOnly, Category="GunDetails")
	int CurrentBulletInMagazine;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category="GunDetails")
	bool bIsOneHanded;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category="GunDetails")
	FName GunName;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category="GunDetails")
	FPointDamageEvent PointDamageEvent;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category="GunDetails")
	UAudioComponent* AudioComponent;
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	virtual void GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const override;



public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	void Reload();

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	void BP_FireFeedback(bool IsReloading, bool IsPrimaryShoot);
	
};
