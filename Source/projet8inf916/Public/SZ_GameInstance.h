﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Object.h"
#include "SZ_GameInstance.generated.h"

/**
 * 
 */
UCLASS()
class PROJET8INF916_API USZ_GameInstance : public UGameInstance
{
	GENERATED_BODY()

	

public:

	virtual void Init() override;

	UFUNCTION(BlueprintImplementableEvent)
	void BeginLoadingScreen(const FString& MapName);

	UFUNCTION(BlueprintImplementableEvent)
	void EndLoadingScreen(UWorld* LoadedWorld);

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category="Number of player")
	int NbPlayerInGame;

	UFUNCTION(BlueprintPure)
	bool IsAzertyKeyboard();
};
