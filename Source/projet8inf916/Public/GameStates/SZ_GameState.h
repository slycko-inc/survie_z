﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameState.h"
#include "UObject/Object.h"
#include "SZ_GameState.generated.h"

/**
 * 
 */
UCLASS()
class PROJET8INF916_API ASZ_GameState : public AGameState
{
public:
	virtual float GetServerWorldTimeSeconds() const override;

private:
	GENERATED_BODY()
};
