﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SZ_GameState.h"
#include "UObject/Object.h"
#include "SZ_WaveTimerGameState.generated.h"


DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FWaveTimeDelegate, float, WaveTime, float, RemainingTime);

/**
 * 
 */
UCLASS()
class PROJET8INF916_API ASZ_WaveTimerGameState : public ASZ_GameState
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintAssignable)
	FWaveTimeDelegate OnWaveTimeReplicated;

	float GetWaveTime() const { return WaveTime; };
	void SetWaveTime(float Time); 

protected:

	virtual void GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const override;

	UFUNCTION()
	void OnRep_WaveTime();
	
	UPROPERTY(BlueprintReadOnly, ReplicatedUsing=OnRep_WaveTime)
	float WaveTime = 0.0f;
};
