﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SZ_TPSCharacter.h"
#include "GameFramework/Actor.h"
#include "GameFramework/Character.h"
#include "SZ_BaseEnemy.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FSZOnDeathDelegate);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FSZOnTakeDamageDelegate, float, DamageTaken);


UENUM()
enum class EEnemyState : uint8
{
	Alive,
	Dead
};

UCLASS()
class PROJET8INF916_API ASZ_BaseEnemy : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ASZ_BaseEnemy();

	UPROPERTY(ReplicatedUsing=OnRep_Life,BlueprintReadWrite, Category="Stats")
	float Life;

	UPROPERTY(BlueprintAssignable)
	FSZOnDeathDelegate OnDeathDelegate;
	
	UPROPERTY(BlueprintAssignable)
	FSZOnTakeDamageDelegate OnTakeDamage;

	UFUNCTION(BlueprintImplementableEvent)
	void BP_TakeDamage(AActor* Damager);
	
	UFUNCTION(BlueprintImplementableEvent)
	void BP_DeathCauser(ASZ_TPSCharacter* Damager);
	
	UFUNCTION(BlueprintImplementableEvent)
	void BP_LifeChange(float OldLife, float NewLife);

	// Called every frame
	virtual void Tick(float DeltaTime) override;
	
	virtual float TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator,
		AActor* DamageCauser) override;
	void TakeExplosionDamage(float Damage, FVector Vector,AActor* DamageCauser);

	void TakeGenericDamage(float Damage,AActor* DamageCauser);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	virtual void GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const override;

	UPROPERTY(ReplicatedUsing=OnRep_State, BlueprintReadWrite)
	EEnemyState State;

	UFUNCTION()
	void OnRep_State() const;
	UFUNCTION()
	void OnRep_Life(float OldLife);

	void DecreaseLife(float Delta);
private:
	
	UFUNCTION()
	void TakePointDamage(AActor* DamagedActor, float Damage, AController* InstigatedBy, FVector HitLocation, UPrimitiveComponent* FHitComponent, FName BoneName, FVector ShotFromDirection, const UDamageType* DamageType, AActor* DamageCauser);

	UFUNCTION()
	void TakeRadialDamage(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, FVector Origin, FHitResult HitInfo, class AController* InstigatedBy, AActor* DamageCauser );
};
