﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameMode.h"
#include "SZ_LobbyGameMode.generated.h"

/**
 * 
 */
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnPostLoginDelegate);
UCLASS()
class PROJET8INF916_API ASZ_LobbyGameMode: public AGameMode
{
	GENERATED_BODY()
protected:
	virtual void OnPostLogin(AController* NewPlayer) override;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	int MinimumNumberOfPlayers = 4;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	bool bAutoLoadGameWorld = true;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FString GameWorldName;

public:
	UPROPERTY(BlueprintAssignable)
	FOnPostLoginDelegate OnPostLoginDelegate;

	UFUNCTION(BlueprintCallable)
	void TravelToGame();
};
