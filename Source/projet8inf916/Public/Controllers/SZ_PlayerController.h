﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Object.h"
#include "SZ_PlayerController.generated.h"

/**
 * 
 */
UCLASS()
class PROJET8INF916_API ASZ_PlayerController : public APlayerController
{
private:
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable, BlueprintPure)
	float GetServerTime() const { return ServerTime; };
	
	virtual void ReceivedPlayer() override;

	virtual void Tick(float DeltaSeconds) override;

protected:

	UFUNCTION(Server, Reliable)
	void ServerRequestServerTime(float RequestWorldTime);

	UFUNCTION(Client, Reliable)
	void ClientResponseServerTime(float RequestWorldTime, float CurrentServerTime);
	
	float ServerTime = 0;
};
