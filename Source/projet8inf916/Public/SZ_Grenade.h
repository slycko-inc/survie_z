﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/SphereComponent.h"
#include "Engine/DamageEvents.h"
#include "UObject/Object.h"
#include "SZ_Grenade.generated.h"

/**
 * 
*/
UENUM()
enum class EGrenadeState : uint8
{
	Ready,
	Exploded
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FbisOnExploded);
UCLASS()
class PROJET8INF916_API ASZ_Grenade : public AActor
{
	GENERATED_BODY()

private:

	UFUNCTION()
	bool ExecuteSweep(TArray<FHitResult>& OutResults,FRadialDamageEvent DamageEvent);
	
protected:
	virtual void BeginPlay() override;

	UPROPERTY(ReplicatedUsing=OnRep_State, BlueprintReadWrite)
	EGrenadeState State;

	UFUNCTION()
	void OnRep_State() const;

public:

	ASZ_Grenade();

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	UPROPERTY()
	FRadialDamageEvent RadialDamageEvent;

	UFUNCTION()
	void OnExplode();
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	USphereComponent* SphereComponent;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	USphereComponent* SphereComponentGrenadeRange;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	UStaticMeshComponent* StaticMeshComponent;
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category="GrenadeDetails")
	FRadialDamageParams RadialDamageParams;

	UPROPERTY(BlueprintAssignable)
	FbisOnExploded bisOnExploded;
	
};
