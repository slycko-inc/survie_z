# SurvieZ
A UE 5 game made during courses of Master in Computers Sciences at University of Quebec at Chicoutimi, at the winter/summer 2023 trimestral. 

## Summary 
Are you ready? A zombie horde is coming, we have to act! SurvieZ is a multiplayer game for up to 4 players, who must resist waves of zombies. Between each wave, players can search for equipment such as weapons or ammunition. But don't be fooled, the environment can be full of surprises!

## Core feature

- Custom [Weapon system](https://github.com/SlyckoOrg/Weapon_System-Unreal_Plugin). Can handle melee and ranged weapon
- Walker, Bersek and Runner zombie, made with both behaviour tree and c++ code
- Up to 4 players, using [Epic Online Services](https://dev.epicgames.com/en-US/services) to handle stuff like login, lobby, achievement and more
- Reactive environment that changes every round
  
## Credits

Developpers    

- Marc-Antoine Jean   
- Maxime Cosialls
- Dimitri Varré
